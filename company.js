class Company{
    _id;
    _company;
    _contact;
    _country;

    constructor(paramId,paramCompany,paramContact,paramCountry)
    {
        this._id=paramId;
        this._company=paramCompany;
        this._contact=paramContact;
        this._country=paramCountry;
    }
    
}

var companyClassList=[];

var cp1=new Company(1,"Alfreds Futterkiste","Maria Anders","Germany");
companyClassList.push(cp1);

var cp2= new Company(2,"Centro comercial Moctezuma","Francisco Chang","Mexico");
companyClassList.push(cp2);

var cp3= new Company(3,"Ernst Handel","Roland Mendel","Austria");
companyClassList.push(cp3);

var cp4= new Company(4,"Island Trading","Helen Bennett","UK");
companyClassList.push(cp4);

var cp5= new Company(5,"Laughing Bacchus Winecellars","Yoshi Tannamur","Canada");
companyClassList.push(cp5);

var cp6= new Company(6,"Magazzini Alimentari Riuniti","Giovanni Rovelli","Italy");
companyClassList.push(cp6);

module.exports={companyClassList};