// import  thu vien
const express= require('express');

const {router} = require("./app/routers/company-router");

const app = new express();

const port=8000;

app.use(express.json());

app.use(express.urlencoded({
    extended: true
}));

// Sử dụng router 
app.use("/company", router);

//B4: Start app
app.listen(port, () => {
    console.log(`app listening on port ${port}`);
})





