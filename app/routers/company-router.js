const express = require('express');

const router= express.Router();

const {companyClassList} = require('../../company');

const{getAllCompanyMiddleware} =require("../middlewares/company-middleware");

router.get("/", getAllCompanyMiddleware, (req, res) => {
        res.send(companyClassList);
});

module.exports={router};